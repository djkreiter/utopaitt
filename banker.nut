class Banker
  {
  }

function Banker::GetMaxBankBalance()
{
	local balance = AICompany.GetBankBalance(AICompany.COMPANY_SELF);
	local maxbalance = balance + AICompany.GetMaxLoanAmount() - AICompany.GetLoanAmount();
	return (maxbalance >= balance) ? maxbalance : balance;
}

function Banker::GetMoney(money)
{
	local toloan = AICompany.GetLoanAmount() + money;
	if (AICompany.SetMinimumLoanAmount(toloan)) return true;
	else return false;
}

function Banker::SetMinimumBankBalance(money)
{
    local needed = money - AICompany.GetBankBalance(AICompany.COMPANY_SELF);
	if (needed < 0) return true;
	else {
		if (Banker.GetMoney(needed)) return true;
		else return false;
	}
}

/**
 * Pays back loan if possible, but tries to have at least the loan interval (10,000 pounds)
 * @return True if the action succeeded.
 */
function Banker::PayLoan()
{
	local balance = AICompany.GetBankBalance(AICompany.COMPANY_SELF);
	// overflow protection by krinn
	if (balance + 1 < balance) {
		if (AICompany.SetMinimumLoanAmount(0)) return true;
		else return false;
	}
	local money = 0 - (balance - AICompany.GetLoanAmount()) + 10000;
	if (money > 0) {
		if (AICompany.SetMinimumLoanAmount(money)) return true;
		else return false;
	} else {
		if (AICompany.SetMinimumLoanAmount(0)) return true;
		else return false;
	}
}
