class UtopAI extends AIInfo
 {
   function GetAuthor()      { return "Don Uber"; }
   function GetName()        { return "UtopAI"; }
   function GetDescription() { return "The Recursive One"; }
   function GetVersion()     { return 1; }
   function GetDate()        { return "2021-01-01"; }
   function CreateInstance() { return "UtopAI"; }
   function GetShortName()   { return "UTAI"; }

   function GetSettings()
   {
     AddSetting({name = "max_iter",
                 description = "Maximal iterations through city network",
                 easy_value = 2,
                 medium_value = 3,
                 hard_value = 5,
                 custom_value = 3,
                 flags = 0,
                 min_value = 1,
                 max_value = 10});
   }
 }

 RegisterAI(UtopAI());
