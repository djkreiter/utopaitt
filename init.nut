
class Init{

   president_name = "D. Uber";
   company_name = "UtopAI";

   constructor(){
   }

}


 // Set the company name
 function Init::SetCompanyName()
 {
   if(!AICompany.SetName(company_name)) {
     local i = 2;
     while(!AICompany.SetName(company_name + i)) {
       i = i + 1;
       if(i > 255) break;
     }
   }
   AICompany.SetPresidentName(president_name);
 }


function Init::BuildHQ()
  {
      if (AIMap.IsValidTile(AICompany.GetCompanyHQ(AICompany.COMPANY_SELF))) return;

      local towns = AITownList();
      local HQtown = 0;
      towns.Valuate(AITown.GetPopulation);
      towns.Sort(AIList.SORT_BY_VALUE, false);
      HQtown = towns.Begin();

      local maxRange = Util.sqrt(AITown.GetPopulation(HQtown)/100) + 5;
      local HQArea = AITileList();

      HQArea.AddRectangle(AITown.GetLocation(HQtown) - AIMap.GetTileIndex(maxRange, maxRange), AITown.GetLocation(HQtown) + AIMap.GetTileIndex(maxRange, maxRange));
      HQArea.Valuate(AITile.IsBuildableRectangle, 2, 2);
      HQArea.KeepValue(1);
      HQArea.Valuate(AIMap.DistanceManhattan, AITown.GetLocation(HQtown));
      HQArea.Sort(AIList.SORT_BY_VALUE, true);
      for (local tile = HQArea.Begin(); !HQArea.IsEnd(); tile = HQArea.Next()) {
          if (AICompany.BuildCompanyHQ(tile)) {
              AISign.BuildSign(tile, "UtopAI HQ");
              return HQtown;
          }
      }
      AILog.Warning("No possible HQ location found");
  }
