// TODO: Depot problem when in later iterations (isolated depots on cargo routes)
// TODO: Further debug identified cargo connections
// TODO: Log output data
// TODO: Vehicle number for cargo routes
// TODO: Direction of depot
// TODO: Cargo station building around larger factories/woods problematic
// TODO: Set cargo vehicles to wait till full
// TODO: Max distance/cost for cargo routes
// TODO: Second phase where we also try to bring goods to cities
// TODO: Check if there is already a station at an industry
require("util.nut");
require("init.nut");
require("banker.nut");
import("pathfinder.road", "RoadPathFinder", 4);

class UtopAI extends AIController
 {
   hometown = null;
   BUILDING_FLAG = false;
   CARGO_FLAG = true; /* Flag if AI also should search for industry routes */
   MIN_BUILD_AMOUNT = 20000;
   LAST_CITY = null;
   PRELAST_CITY = null;
   LAST_DEPOT = null;
   LAST_SRCSTATION = null;
   SKIP_LIST = [];
   IMP_ROUTES = [];
   SERVICED_INDUSTRY = [];
   ITERATION = 0;
   FLIPFLOP = 0;
   COUNT = 0;
   MAX_ITER = null;
   MAX_PATH_ITER = 200;
   MAX_PATH_COST = 20000;
   ITER_COST_INCREASE = 10000;
   MAX_DISTANCE = 130;
   vehtobuy = 2;
   matched = {};
   matched_cargo = {};

   constructor()
   {
    Util = Util(this);
    Init = Init();
    Banker = Banker();
   }
 }


 function UtopAI::Start()
 {
   /* Load settings */
   MAX_ITER = AIController.GetSetting("max_iter");

   AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Init] UtopAI starting");
   AIRoad.SetCurrentRoadType(AIRoad.ROADTYPE_ROAD);
   Init.SetCompanyName();
   hometown = Init.BuildHQ();
   AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Init] Building HQ in "+AITown.GetName(hometown));
   if(hometown != null) buildRoute(hometown);

   // Main loop
   while (true) {
        if(Banker.GetMaxBankBalance() > MIN_BUILD_AMOUNT && ITERATION < MAX_ITER){
            buildRoute(LAST_CITY);
        }else{
            // Wait
            AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Finance] Not building because we don't have "+MIN_BUILD_AMOUNT+" OR MAX_ITER reached: "+ITERATION);
            BUILDING_FLAG = false;
        }
        if(!BUILDING_FLAG){
            AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Finance] Paying back loan money");
            Banker.PayLoan();
            AIController.Sleep(20);
        }
        if(AIDate.GetCurrentDate() > 400){
            manageFleet()
            AIController.Sleep(300);
        }
   }
 }

 function UtopAI::Save()
 {
   local table = {
       LAST_CITY = this.LAST_CITY,
       PRELAST_CITY = this.PRELAST_CITY,
       LAST_DEPOT = this.LAST_DEPOT,
       LAST_SRCSTATION = this.LAST_SRCSTATION,
       SKIP_LIST = this.SKIP_LIST,
       ITERATION = this.ITERATION,
       FLIPFLOP = this.FLIPFLOP,
       COUNT = this.COUNT,
       SERVICED_INDUSTRY = this.SERVICED_INDUSTRY,
       hometown = this.hometown
   };
   return table;
 }

 function UtopAI::Load(version, data)
 {
   this.LAST_CITY = data.rawget("LAST_CITY");
   this.PRELAST_CITY = data.rawget("PRELAST_CITY");
   this.LAST_DEPOT = data.rawget("LAST_DEPOT");
   this.LAST_SRCSTATION = data.rawget("LAST_SRCSTATION");
   this.SKIP_LIST = data.rawget("SKIP_LIST");
   this.ITERATION = data.rawget("ITERATION");
   this.FLIPFLOP = data.rawget("FLIPFLOP");
   this.COUNT = data.rawget("COUNT");
   this.hometown = data.rawget("hometown");
   this.SERVICED_INDUSTRY = data.rawget("SERVICED_INDUSTRY");
   AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Init] Loaded");
 }

function UtopAI::buildRoute(src)
{
    local pathfinder = RoadPathFinder();
    local dst = null;
    local production_site = null;
    local accepting = null;
    local area = AITileList();
    local towns = AITownList();
    local industries = AIIndustryList();
    local srcstation = null;
    local dststation = null;
    local depot = null;
    local cargo = null;

    // Fix for if the ID of the town is 0
    if(!src){
        src = 0;
    }
    // Pathfinder configuration
    pathfinder.cost.no_existing_road = 100;
    pathfinder.cost.bridge_per_tile = 250;
    pathfinder.cost.max_bridge_length = 7;

    if(FLIPFLOP == 0){
        FLIPFLOP = 1;
    }else{
        FLIPFLOP = 0;

        // Manage fleet now and then
        if(AIDate.GetCurrentDate() > 400){
            manageFleet()
        }
    }

    if(!(CARGO_FLAG && FLIPFLOP == 0)){

        // Remove already connected towns
        if(ITERATION == 0){
            towns.Valuate(Util.hasStation);
            towns.KeepValue(0);
        }

        // Find other cities shortby
        towns.Valuate(Util.DistanceManhattanFromTown, src);

        local i = 0
        towns.Sort(AIList.SORT_BY_VALUE, true);
        towns.RemoveValue(0); // Remove the source town itself

        while(i < ITERATION){
            towns.RemoveTop(1 + FLIPFLOP);
            i = i + 1;
        }

        if(towns.Count() == 0 || COUNT == AITownList().Count()){
            // All towns connected;
            ITERATION += 1;
            COUNT = 0;
            SKIP_LIST = [];
            LAST_CITY = hometown;
            MAX_PATH_COST += ITER_COST_INCREASE;
            AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Init] Iteration is "+ITERATION);
            BUILDING_FLAG = false;
            return false;
        }

        dst = towns.Begin();

        // Skip towns that were added to the skip list due to errors or impossible paths
        foreach(skipdst in SKIP_LIST){
            if(skipdst == dst){
                AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Pathfinding] Town "+AITown.GetName(dst)+" is in skip list.");
                dst = towns.Next();
            }
        }
        foreach(skiproute in IMP_ROUTES){
            if(src+':'+dst == skiproute){
                AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Pathfinding] Route in skip list.");
                dst = towns.Next();
            }
        }

        // Prevent ending up in a loop
        if(dst == PRELAST_CITY){
            dst = towns.Next();
        }

        // No destinations left to address anymore
        if(dst == null){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] No destination in list left...");
            ITERATION = MAX_ITER; // End route planning
            BUILDING_FLAG = false;
            return false;
        }

        // Evaluate route distnace
        local distance = AIMap.DistanceManhattan(AITown.GetLocation(src), AITown.GetLocation(dst));
        AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Pathfinding] Buidling route to "+AITown.GetName(dst)+" from "+AITown.GetName(src)+" distance: "+distance);
        if(distance > MAX_DISTANCE){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Distance too big.");
            COUNT += 1;
            SKIP_LIST.push(dst);
            IMP_ROUTES.push(src+':'+dst);
            buildRoute(LAST_CITY);
        }
    }else{
        // Fing cargo routes
        src = industries.Begin();
        foreach(serviced in SERVICED_INDUSTRY){
            industries.RemoveItem(serviced);
        }
        if(industries.Count() < 1){
            CARGO_FLAG = false;
            AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Cargofinding] No eligble industries left to service.");
            return false;
        }
        industries.Sort(AIList.SORT_BY_VALUE, true);
        industries.Valuate(Util.getMatchProximity);
        production_site = industries.Begin()
        /*foreach(industry in SERVICED_INDUSTRY){
            if(industry == production_site){
                production_site = industries.Next();
            }
        }*/
        if(production_site == null){
            AILog.Error("No production site");
            return false;
        }
        SERVICED_INDUSTRY.push(production_site);
        accepting = Util.getMatched(production_site);
        if(!accepting){
            AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Cargofinding] No accepting industry found.");
            return false;
        }
        AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Cargofinding] Planning route from "+AIIndustry.GetName(production_site)+" to "+AIIndustry.GetName(accepting)+".");
    }

    // Go in building mode
    BUILDING_FLAG = true;
    Banker.SetMinimumBankBalance(MIN_BUILD_AMOUNT);

    if(!(CARGO_FLAG && FLIPFLOP == 0)){
        if(ITERATION == 0){
            LAST_SRCSTATION = Util.getTownStation(src);
            srcstation = LAST_SRCSTATION;
            if(srcstation == null) srcstation = buildStation(src);
            dststation = buildStation(dst);
        }else{
            srcstation = Util.getTownStation(src);
            dststation = Util.getTownStation(dst);
            if(srcstation == null) srcstation = buildStation(src);
            if(dststation == null) dststation = buildStation(dst);
            //depot = Util.findDepot(src);
        }
        if(srcstation == null || dststation == null){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] There was not station in "+AITown.GetName(src)+" or "+AITown.GetName(dst));
            COUNT += 1;
            SKIP_LIST.push(dst);
            return false;
        }
    }else{
        // Build station at production site
        srcstation = buildCargoStation(production_site);

        // Build station at accepting site
        dststation = buildCargoStation(accepting);
    }
    if(!srcstation || !dststation){
        return false;
    }
    // Start pathfinding
    pathfinder.InitializePath([srcstation], [dststation]);
    local path = false;
    AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Pathfinding] Iterating path");
    local iters = 0;
    while (path == false) {
        path = pathfinder.FindPath(100);
        iters += 1;
        if(iters > MAX_PATH_ITER){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Route too hard to find. Skipping.");
            COUNT += 1;
            SKIP_LIST.push(dst);
            IMP_ROUTES.push(src+':'+dst);
            buildRoute(LAST_CITY);
        }
    }
    local cost = 0;
    if(path){
        cost = path.GetCost();
    }else{
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] No path found.");
        COUNT += 1;
        SKIP_LIST.push(dst);
        IMP_ROUTES.push(src+':'+dst);
        buildRoute(LAST_CITY);
    }
    if(cost > MAX_PATH_COST){
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Route too expensive");
        SKIP_LIST.push(dst);
        COUNT += 1;
        buildRoute(LAST_CITY);
    }
    if(cost > Banker.GetMaxBankBalance()){
        return null;
    }
    if(cost > AICompany.GetBankBalance(AICompany.COMPANY_SELF)){
        if(!Banker.GetMoney(cost)){
            return null;
        }
    }
    if(buildPath(path)){
        AILog.Warning("Trying to build depot for station: "+srcstation);
        try {
            if(CARGO_FLAG && FLIPFLOP == 0 && !depot && srcstation){
                depot = buildRoadDepot(srcstation, false);
            }
            if(!depot && dst) depot = buildRoadDepot(dst, true);
            if(depot == null && src) depot = buildRoadDepot(src, true);
            if(depot == null && srcstation) depot = buildRoadDepot(srcstation, false);
            if(depot == null && dststation) depot = buildRoadDepot(dststation, false);
        }catch(exception){
            if(CARGO_FLAG && FLIPFLOP == 0 && !depot && dststation){
                depot = buildRoadDepot(dststation, false);
            }
            if(LAST_DEPOT != null){
                depot = LAST_DEPOT;
            }else{
                depot = buildRoadDepot(src, true);
            }

        }

        // Calculate how many vehicles to use in this service
        if(!(CARGO_FLAG && FLIPFLOP == 0) && src != null && dst != null && srcstation != null && dststation != null && depot != null){
            AILog.Warning("Starting service...");
            vehtobuy = ((AITown.GetPopulation(src) + AITown.GetPopulation(dst))/500).tointeger()
            if(vehtobuy < 2) vehtobuy = 2;
            // Start the service
            if(startService(srcstation, dststation, depot, "p")){
                PRELAST_CITY = LAST_CITY;
                COUNT += 1;
                LAST_CITY = dst;
                if(Banker.GetMaxBankBalance() > MIN_BUILD_AMOUNT){
                    Banker.SetMinimumBankBalance(MIN_BUILD_AMOUNT);
                    buildRoute(LAST_CITY);
                }else{
                    AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Finance] Not enough money. We will wait.");
                    return null;
                }
            }else{
                AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Failed to start service");
                return null;
            }
        }else if(production_site != null){
            vehtobuy = 2 // TODO: TO UPDATE, ADAPTIVE
            cargo = Util.getMatchedCargo(production_site);
            if(startService(srcstation, dststation, depot, cargo)){
                if(Banker.GetMaxBankBalance() > MIN_BUILD_AMOUNT){
                    Banker.SetMinimumBankBalance(MIN_BUILD_AMOUNT);
                    buildRoute(LAST_CITY);
                }else{
                    AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Finance] Not enough money. We will wait.");
                    return null;
                }
            }else{
                AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Failed to start service");
                return null;
            }
        }else{
            if(Banker.GetMaxBankBalance() > MIN_BUILD_AMOUNT){
                Banker.SetMinimumBankBalance(MIN_BUILD_AMOUNT);
                buildRoute(LAST_CITY);
            }else{
                AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Finance] Not enough money. We will wait.");
                return null;
            }
        }

    }else{
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Path building has failed");
        return null;
    }
}


function UtopAI::buildRoadDepot(loc, isTown){
    AILog.Error("isTown "+isTown);
    local buildArea = AITileList();
    local maxRange = 3;
    if(isTown == true){
        maxRange = Util.sqrt(AITown.GetPopulation(loc)/100) + 5;
        buildArea.AddRectangle(AITown.GetLocation(loc) - AIMap.GetTileIndex(maxRange, maxRange), AITown.GetLocation(loc) + AIMap.GetTileIndex(maxRange, maxRange));
    }else{
        buildArea.AddRectangle(loc - AIMap.GetTileIndex(maxRange, maxRange), loc + AIMap.GetTileIndex(maxRange, maxRange));
    }
    buildArea.Valuate(Util.hasConnectableRoad, false);
    buildArea.RemoveValue(-1);
    local buildTile = buildArea.Begin();
    local connectTile = Util.hasConnectableRoad(buildTile, false)
    if(connectTile == null){
        AILog.Error("An unexpeceted error with the connect tile for the road depot");
    }
    if(!AIRoad.BuildRoadDepot(buildTile, connectTile)){
        local depotbuilt = false;
        local i = 0;
        while(!depotbuilt){
        local newBuildArea = buildArea.Next();
        if(newBuildArea == null) return LAST_DEPOT;
        local connectTile = Util.hasConnectableRoad(newBuildArea, false)
        if(!connectTile) continue;
         if(AIRoad.BuildRoadDepot(newBuildArea, connectTile)){
            if(!AIRoad.AreRoadTilesConnected(newBuildArea, connectTile)){
                AIRoad.BuildRoad(newBuildArea, connectTile);
            }
            LAST_DEPOT = newBuildArea;
            return newBuildArea;
            }
            i = i + 1;
            if(i > 10){
                AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Planning] Depot building really failed ");
                break;
            }
        }
    }else{
        if(!AIRoad.AreRoadTilesConnected(buildArea.Begin(), connectTile)){
            AIRoad.BuildRoad(buildArea.Begin(), connectTile);
        }
        LAST_DEPOT = buildArea.Begin();
    }
    return LAST_DEPOT;
}

function UtopAI::buildStation(town){
    AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Planning] Building in town "+AITown.GetName(town));
    local maxRange = Util.sqrt(AITown.GetPopulation(town)/100) + 5;
    local buildArea = AITileList();
    buildArea.AddRectangle(AITown.GetLocation(town) - AIMap.GetTileIndex(maxRange, maxRange), AITown.GetLocation(town) + AIMap.GetTileIndex(maxRange, maxRange));
    buildArea.Valuate(canBuildStation);
    buildArea.KeepValue(1);
    buildArea.Valuate(AIMap.DistanceManhattan, AITown.GetLocation(town));
    buildArea.Sort(AIList.SORT_BY_VALUE, true);
    try {
    if(!AIRoad.BuildDriveThroughRoadStation(buildArea.Begin(), Util.IsDriveThrough(buildArea.Begin(), false), AIRoad.ROADVEHTYPE_BUS, AIStation.STATION_NEW)){
        if(!Util.hasStation(town)){
            AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Pathfinding] Station can't be build, trying another place.");
        }else{
            return buildArea.Begin();
        }
        local i = 0;
        while(!Util.hasStation(town)){
         local newBuildArea = buildArea.Next();
         if(AIRoad.BuildDriveThroughRoadStation(newBuildArea, Util.IsDriveThrough(newBuildArea, false), AIRoad.ROADVEHTYPE_BUS, AIStation.STATION_NEW)){
            LAST_SRCSTATION = newBuildArea;
            return LAST_SRCSTATION;
         }
         i = i + 1;
         if(i > 5){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] Station building really failed");
            return false;
         }
        }
    }else{
        LAST_SRCSTATION = buildArea.Begin();
        return LAST_SRCSTATION;
    }
    }catch(exception){
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] An error occurred building this station... Aborting this route");
        return LAST_SRCSTATION;
    }
}

function UtopAI::buildCargoStation(site){
    AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Planning] Building at Industry "+AIIndustry.GetName(site));
    local buildArea = AITileList();
    local connect_buildArea = AITileList();
    local maxRange = 5;
    buildArea.AddRectangle(AIIndustry.GetLocation(site) - AIMap.GetTileIndex(maxRange, maxRange), AIIndustry.GetLocation(site) + AIMap.GetTileIndex(maxRange, maxRange));
    buildArea.Valuate(AITile.IsBuildable);
    buildArea.KeepValue(1);
    buildArea.Valuate(AIMap.DistanceManhattan, AIIndustry.GetLocation(site));
    buildArea.Sort(AIList.SORT_BY_VALUE, true);
    local buildTile = buildArea.Begin();
    local connectTile = Util.hasConnectableBuild(buildTile);
    local pre_count = AIIndustry.GetAmountOfStationsAround(site);
    local j = 0;
    while(j < maxRange*maxRange-1){
        buildTile = buildArea.Next();
        connectTile = Util.hasConnectableBuild(buildTile);
        if(connectTile != null){
            break;
        }
        j = j + 1;
    }
    if(!connectTile){
        AILog.Error("No road connection possible... Aborting.")
        return false;
    }
    try {
    if(!AIRoad.BuildRoadStation(buildTile, connectTile, AIRoad.ROADVEHTYPE_TRUCK, AIStation.STATION_NEW)){
        local post_count = AIIndustry.GetAmountOfStationsAround(site);
        if(pre_count == post_count){
            AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Cargofinding] Cargo station can't be build, trying another place.");
        }else{
            if(!AIRoad.AreRoadTilesConnected(buildTile, connectTile)){
                AIRoad.BuildRoad(buildTile, connectTile);
            }
            return buildTile;
        }
        local i = 0;
        while(pre_count == post_count){
         buildTile = buildArea.Next();
         connectTile = Util.hasConnectableBuild(buildTile);
         if(AIRoad.BuildRoadStation(buildTile, connectTile, AIRoad.ROADVEHTYPE_TRUCK, AIStation.STATION_NEW)){
            if(!AIRoad.AreRoadTilesConnected(buildTile, connectTile)){
                AIRoad.BuildRoad(buildTile, connectTile);
            }
            return buildTile;
         }
         i = i + 1;
         if(i > 20){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Cargofinding] Cargo station building really failed");
            return false;
         }
        }
    }else{
        if(!AIRoad.AreRoadTilesConnected(buildTile, connectTile)){
            AIRoad.BuildRoad(buildTile, connectTile);
        }
        return buildTile;
    }
    }catch(exception){
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Cargofinding] An error occurred building this cargo station... Aborting this route");
        return null;
    }
}

function UtopAI::canBuildStation(tile){
    return AIRoad.IsRoadTile(tile) && Util.IsDriveThrough(tile, true);
}


function UtopAI::buildPath(path){
AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Pathfinding] Building path");
while (path != null) {
  local par = path.GetParent();
  if (par != null) {
    local last_node = path.GetTile();
    if (AIMap.DistanceManhattan(path.GetTile(), par.GetTile()) == 1 ) {
      local isbuild = false;
      local i = 0;
      while (!isbuild) {
        if(!AIRoad.BuildRoad(path.GetTile(), par.GetTile())){
           if(AIError.GetLastError() == AIError.ERR_ALREADY_BUILT){
               isbuild = true;
               break;
           }
           AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Pathfinding] A part of the road could not be build, trying again...");
           AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] "+AIError.GetLastErrorString());
           if(AIError.GetLastError() == AIError.ERR_NOT_ENOUGH_CASH){
               AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Warning][Finance] Not enough money, trying to get loan");
               if(!Banker.GetMoney(20000)){
                    AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Finance] No money available, waiting for money");
                    AIController.Sleep(100);
               }
           }
           i = i + 1;
        }else{
           isbuild = true;
           break;
        }
        if(i > 20){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Pathfinding] The road could not be build, sorry...");
            return false;
            // TODO: Handle complete failure
        }
        AIController.Sleep(1);
      }
    } else {
      /* Build a bridge or tunnel. */
      if (!AIBridge.IsBridgeTile(path.GetTile()) && !AITunnel.IsTunnelTile(path.GetTile())) {
        /* If it was a road tile, demolish it first. Do this to work around expended roadbits. */
        if (AIRoad.IsRoadTile(path.GetTile())) AITile.DemolishTile(path.GetTile());
        if (AITunnel.GetOtherTunnelEnd(path.GetTile()) == par.GetTile()) {
          if (!AITunnel.BuildTunnel(AIVehicle.VT_ROAD, path.GetTile())) {
            /* An error occured while building a tunnel. TODO: handle it. */
          }
        } else {
          local bridge_list = AIBridgeList_Length(AIMap.DistanceManhattan(path.GetTile(), par.GetTile()) + 1);
          bridge_list.Valuate(AIBridge.GetMaxSpeed);
          bridge_list.Sort(AIList.SORT_BY_VALUE, false);
          if (!AIBridge.BuildBridge(AIVehicle.VT_ROAD, bridge_list.Begin(), path.GetTile(), par.GetTile())) {
            /* An error occured while building a bridge. TODO: handle it. */
          }
        }
      }
    }
  }
  path = par;
}
return true;
}

function UtopAI::ChooseRoadVeh(cargo)
  {
      local vehlist = AIEngineList(AIVehicle.VT_ROAD);
      vehlist.Valuate(AIEngine.GetRoadType);
      vehlist.KeepValue(AIRoad.ROADTYPE_ROAD);
      // Exclude articulated vehicles
      vehlist.Valuate(AIEngine.IsArticulated);
      vehlist.KeepValue(0);
      // Remove zero cost cars
      vehlist.Valuate(AIEngine.GetPrice);
      vehlist.RemoveValue(0);
      // Filter by cargo
      vehlist.Valuate(AIEngine.CanRefitCargo, cargo);
      vehlist.KeepValue(1);
      // Valuate the vehicles using krinn's valuator
      vehlist.Valuate(GetEngineRawEfficiency, cargo, true);
      vehlist.Sort(AIList.SORT_BY_VALUE, AIList.SORT_ASCENDING);
      local veh = vehlist.Begin();
      if (vehlist.Count() == 0) veh = null;
      return veh;
  }

function UtopAI::GetEngineRawEfficiency(engine, cargoID, fast)
  {
      local price = AIEngine.GetPrice(engine);
      local capacity = AIEngine.GetCapacity(engine);
      local speed = AIEngine.GetMaxSpeed(engine);
      local lifetime = AIEngine.GetMaxAge(engine);
      local runningcost = AIEngine.GetRunningCost(engine);
      if (capacity <= 0)  return 9999999;
      if (price <= 0) return 9999999;
      local eff = 0;
      if (fast)   eff = 1000000 / ((capacity*0.9)+speed).tointeger();
          else    eff = 1000000-(capacity * speed);
      return eff;
  }

function UtopAI::startService(src, dst, depot, cargo){
    if(cargo == "p"){
        cargo = Util.Passengers();
    }
    local veh = ChooseRoadVeh(cargo);
    if (veh == null) {
                      AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicle] No suitable road vehicle available!");
                      return false;
                  } else {
                      AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicle] Selected road vehicle: " + AIEngine.GetName(veh));
                  }
    BuildAndStartVehicles(src, dst, veh, vehtobuy, depot, null);
    return true;
    }

function UtopAI::BuildAndStartVehicles(src, dst, veh, number, depot, ordervehicle)
  {
      local srcplace = src;
      local dstplace = dst;
      local homedepot = depot;
      local crg = Util.Passengers();
      local price = AIEngine.GetPrice(veh);
      // Check if we have enough money
      if (AICompany.GetBankBalance(AICompany.COMPANY_SELF) < price) {
          if (!Banker.SetMinimumBankBalance(price)) {
              return false;
          }
      }
      // Build and refit the first vehicle
      local firstveh = AIVehicle.BuildVehicle(homedepot, veh);
      if(!AIVehicle.IsValidVehicle(firstveh)){
        AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Vehicle] "+AIError.GetLastErrorString());
      }
      if (ordervehicle == null) {
          // If there is no other vehicle to share orders with
          local firstorderflag = null;
          local secondorderflag = AIOrder.OF_NON_STOP_INTERMEDIATE;
          // Non-stop is not needed for planes
          if (AIEngine.GetVehicleType(veh) == AIVehicle.VT_AIR) firstorderflag = secondorderflag = AIOrder.OF_NONE;
          else {
              if (AICargo.GetTownEffect(crg) == AICargo.TE_PASSENGERS || AICargo.GetTownEffect(crg) == AICargo.TE_MAIL) {
                  firstorderflag = AIOrder.OF_NON_STOP_INTERMEDIATE;
              } else {
                  firstorderflag = AIOrder.OF_FULL_LOAD_ANY + AIOrder.OF_NON_STOP_INTERMEDIATE;
              }
          }
          AIOrder.AppendOrder(firstveh, srcplace, firstorderflag);
          AIOrder.AppendOrder(firstveh, dstplace, secondorderflag);
      } else {
          AIOrder.ShareOrders(firstveh, ordervehicle);
      }
      AIVehicle.StartStopVehicle(firstveh);
      AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicle] Building vehicle 1/"+number);
      for (local idx = 2; idx <= number; idx++) {
          // Clone the first vehicle if we need more than one vehicle
          if (AICompany.GetBankBalance(AICompany.COMPANY_SELF) < price) {
              Banker.SetMinimumBankBalance(price);
          }
          local nextveh = AIVehicle.CloneVehicle(homedepot, firstveh, true);
          AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicle] Building vehicle "+idx+"/"+number);
          AIVehicle.StartStopVehicle(nextveh);
          AIOrder.SkipToOrder(nextveh, 2); // Skip to next order to variate
      }
      return true;
  }

function UtopAI::manageFleet(){
    AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicles] Mananging fleet");
    local vehicleIDs = AIVehicleList();
    local newvehtype = ChooseRoadVeh(Util.Passengers());
    for (local veh = vehicleIDs.Begin(); vehicleIDs.HasNext(); veh = vehicleIDs.Next()) {
        if(AIVehicle.GetAge(veh) > 480){
            local losing = (AIVehicle.GetProfitLastYear(veh) < 1).tointeger() & (AIVehicle.GetProfitThisYear(veh) < 1).tointeger();
            if(losing){
                AILog.Warning(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicles] "+veh+" is losing money");
                if(!AIVehicle.IsStoppedInDepot(veh)) AIVehicle.SendVehicleToDepot(veh) else AIVehicle.SellVehicle(veh);
            }
        }
        if(AIVehicle.GetAgeLeft(veh) < 5 || AIVehicle.GetReliability(veh) < 50){
            AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Vehicles] Vehicle #"+veh+" needs to retire");
            if(!AIVehicle.IsStoppedInDepot(veh)){
                AIVehicle.SendVehicleToDepot(veh)
            }else{
                local newveh = AIVehicle.BuildVehicle(AIVehicle.GetLocation(veh), newvehtype)
                if(!newveh){
                    AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Vehicles] Could not replace old vehicle");
                }else{
                    AIVehicle.StartStopVehicle(newveh);
                }
                AIVehicle.SellVehicle(veh);
            }
        }else{
            if(AIVehicle.IsStoppedInDepot(veh) && !losing){
                AIVehicle.StartStopVehicle(veh);
            }
        }
    }
    return;
}

