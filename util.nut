class Util
  {
    matched = {};
    matched_cargo = {};
    root = null;
    constructor(that){
        root = that;
        matched = that.matched;
    }
    function setMatched(producing, accepting){
        try {
            matched[producing] <- accepting;
        }catch(exp){
            return null;
        }
    }
    function setMatchedCargo(producing, cargo){
        try {
            matched_cargo[producing] <- cargo;
        }catch(exp){
            return null;
        }
    }
    function getMatched(producing){
        try {
            return matched[producing];
        }catch(exp){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Cargofinding] Match not found in table...")
            return null;
        }
    }
    function getMatchedCargo(producing){
        try {
            return matched_cargo[producing];
        }catch(exp){
            AILog.Error(AIDate.GetCurrentDate()+" [UtopAI][Error][Cargofinding] Cargo match not found in table...")
            return null;
        }
    }
  }

function Util::sqrt(i) //From Rondje
  {
      if (i == 0) return 0; //Avoid divide by zero
      local n = (i / 2) + 1; //Initial estimate, never low
      local n1 = (n + (i / n)) / 2;
      while (n1 < n) {
          n = n1;
          n1 = (n + (i / n)) / 2;
      }
      return n;
  }


function Util::printTownList(){
    local towns = AITownList();
    foreach(town, value in towns){
        AILog.Info(town+" : "+AITown.GetName(town)+" "+AITown.GetPopulation(town));
    }
}

function Util::DistanceManhattanFromTown(src, dst){
    local src_tile = AITown.GetLocation(src);
    local dst_tile = AITown.GetLocation(dst);
    return AIMap.DistanceManhattan(src_tile, dst_tile);
}

function Util::DistanceManhattanFromIndustry(src, dst){
    local src_tile = AIIndustry.GetLocation(src);
    local dst_tile = AIIndustry.GetLocation(dst);
    return AIMap.DistanceManhattan(src_tile, dst_tile);
}

function Util::IsDriveThrough(tile, want_bool){
    local N = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)+1);
    local S = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)-1);
    local W = AIMap.GetTileIndex(AIMap.GetTileX(tile)-1, AIMap.GetTileY(tile));
    local E = AIMap.GetTileIndex(AIMap.GetTileX(tile)+1, AIMap.GetTileY(tile));
    if(AIRoad.IsRoadTile(N) && AIRoad.IsRoadTile(S) && !AIRoad.IsRoadTile(W) && !AIRoad.IsRoadTile(E)){
        if(!want_bool){
            return N;
        }else{
            return true;
        }
    }
    if(AIRoad.IsRoadTile(W) && AIRoad.IsRoadTile(E) && !AIRoad.IsRoadTile(N) && !AIRoad.IsRoadTile(S)){
        if(!want_bool){
            return E;
        }else{
            return true;
        }
    }
    return false;
}

function Util::hasConnectableRoad(tile, want_bool){
    local N = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)+1);
    local S = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)-1);
    local W = AIMap.GetTileIndex(AIMap.GetTileX(tile)-1, AIMap.GetTileY(tile));
    local E = AIMap.GetTileIndex(AIMap.GetTileX(tile)+1, AIMap.GetTileY(tile));
    if(want_bool){
        if((AIRoad.IsRoadTile(N) && Util.sameHeight(tile, N)) || (AIRoad.IsRoadTile(S) && Util.sameHeight(tile, S)) || (AIRoad.IsRoadTile(W) && Util.sameHeight(tile, W)) || (AIRoad.IsRoadTile(E) && Util.sameHeight(tile, E))){
            return 1;
        }else{
            return 0;
        }
    }
    if(AIRoad.IsRoadTile(N) && Util.sameHeight(tile, N)){
        return N;
    }
    if(AIRoad.IsRoadTile(S) && Util.sameHeight(tile, S)){
        return S;
    }
    if(AIRoad.IsRoadTile(W) && Util.sameHeight(tile, W)){
        return W;
    }
    if(AIRoad.IsRoadTile(E) && Util.sameHeight(tile, E)){
        return E;
    }
    return -1;
}

function Util::hasConnectableBuild(tile){
    local N = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)+1);
    local S = AIMap.GetTileIndex(AIMap.GetTileX(tile), AIMap.GetTileY(tile)-1);
    local W = AIMap.GetTileIndex(AIMap.GetTileX(tile)-1, AIMap.GetTileY(tile));
    local E = AIMap.GetTileIndex(AIMap.GetTileX(tile)+1, AIMap.GetTileY(tile));
    if(AITile.IsBuildable(N) && Util.sameHeight(tile, N)){
        return N;
    }
    if(AITile.IsBuildable(S) && Util.sameHeight(tile, S)){
        return S;
    }
    if(AITile.IsBuildable(W) && Util.sameHeight(tile, W)){
        return W;
    }
    if(AITile.IsBuildable(E) && Util.sameHeight(tile, E)){
        return E;
    }
    return null;
}

function Util::sameHeight(tile1, tile2){
    if(AITile.GetMinHeight(tile1) == AITile.GetMinHeight(tile2) && AITile.GetMaxHeight(tile1) == AITile.GetMaxHeight(tile2) && AITile.GetMinHeight(tile1) == AITile.GetMaxHeight(tile2)){
        return 1;
    }
}

function Util::Passengers()
  {
      local cargolist = AICargoList();
      foreach (cargo, dummy in cargolist) {
          if (AICargo.GetTownEffect(cargo) == AICargo.TE_PASSENGERS) return cargo;
      }
      return null;
  }

function Util::hasStation(town){
    local stationlist = AIStationList(AIStation.STATION_BUS_STOP);
    stationlist.Valuate(AIStation.IsWithinTownInfluence, town);
    stationlist.KeepValue(1);
    if(stationlist.Count() > 0){
        return 1;
    }else{
        return 0;
    }
}

function Util::getTownStation(town){
    local stationlist = AIStationList(AIStation.STATION_BUS_STOP);
    stationlist.Valuate(AIStation.IsWithinTownInfluence, town);
    stationlist.KeepValue(1);
    if(stationlist.Count() > 0){
        return AIStation.GetLocation(stationlist.Begin());
    }else{
        return null;
    }
}

function Util::findDepot(town){
    local depotlist = AIDepotList(AITile.TRANSPORT_ROAD);
    depotlist.Valuate(AIMap.DistanceManhattan, AITown.GetLocation(town));
    depotlist.Sort(AIList.SORT_BY_VALUE, true);
    return depotlist.Begin();
}

function Util::getMatchProximity(industry){
    local current_value = 9999998;
    if(AIIndustry.IsBuiltOnWater(industry)){
        return 9999999;
    }
    local type = AIIndustry.GetIndustryType(industry);
    if(!AIIndustryType.IsRawIndustry(type)){
        return 9999999;
    }
    local produced_cargo = AIIndustryType.GetProducedCargo(type);
    if(produced_cargo.IsEmpty()){
        return 9999999;
    }
    AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Cargofinding] Checking "+AIIndustry.GetName(industry)+", type: "+AIIndustryType.GetName(type));
    foreach(cargo, dummy in produced_cargo){
         if(!AICargo.IsValidCargo(cargo) || cargo == 0){
            continue;
         }
         AILog.Warning("cargo"+cargo);
         local accepting = AIIndustryList_CargoAccepting(cargo)
         accepting.Valuate(Util.DistanceManhattanFromIndustry, industry)
         AILog.Warning("count"+accepting.Count());
         local new_value = accepting.GetValue(accepting.Begin());
         //AILog.Info(AIDate.GetCurrentDate()+" [UtopAI][Info][Cargofinding] Checking "+AIIndustry.GetName(accepting.Begin())+" as receiving industry.");
         if(new_value < current_value){
            current_value = new_value;
            Util.setMatched(industry, accepting.Begin()); // TODO: THIS DOES NOT WORK. CREATE TABLE TO MATCH ORIGIN TO MATCH
            Util.setMatchedCargo(industry, cargo);
            AILog.Warning("found "+AIIndustry.GetName(Util.getMatched(industry)));
         }
    }
    return current_value;
}
